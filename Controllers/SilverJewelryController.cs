﻿using Microsoft.AspNetCore.Mvc;
using Services.Model;
using Services.Services.Interfaces;

namespace PRN231PE_FA23_665511_SE160605.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class SilverJewelryController : ControllerBase
    {
        private readonly ISilverJewelryService _silverJewelryService;

        public SilverJewelryController(ISilverJewelryService silverJewelryService)
        {
            _silverJewelryService = silverJewelryService;
        }

        [HttpGet]
        public async Task<ActionResult> GetAllSilverJewelry() => Ok(await _silverJewelryService.GetAllSilverJewelry());

        [HttpGet("SilverJewelry")]
        public async Task<ActionResult> SearchSilverJewelry([FromQuery] SilverJewelrySerchModel silverJewelrySerchModel) => Ok(await _silverJewelryService.SearchSilverJewelry(silverJewelrySerchModel));

        [HttpDelete("{id}")]
        public async Task<ActionResult> DeleteSilverJewelry(string id) => Ok(await _silverJewelryService.DeleteSilverJewelry(id));

        [HttpPost]
        public async Task<ActionResult> CreateSilverJewelry(CreateSilverJewelryModel model) => Ok(await _silverJewelryService.CreateSilverJewelry(model));
        [HttpPut]
        public async Task<ActionResult> UpdateSilverJewelry(UpdateSilverJewelryModel model) => Ok(await _silverJewelryService.UpdateSilverJewelry(model));
    }
}
