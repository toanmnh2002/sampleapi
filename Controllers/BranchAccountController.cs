﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Services.Model;
using Services.Services.Interfaces;


namespace PRN231PE_FA23_665511_SE160605.Controllers
{
    [Route("Authentication")]
    [ApiController]
    [AllowAnonymous]
    public class BranchAccountController : ControllerBase
    {
        private readonly IBranchAccountService _branchAccountService;

        public BranchAccountController(IBranchAccountService branchAccountService)
        {
            _branchAccountService = branchAccountService;
        }
        [HttpPost]
        public async Task<ActionResult> Login([FromBody] LoginModel model)
        {
            var result = await _branchAccountService.Login(model);
            return Ok(result);
        }
    }
}
