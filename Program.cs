using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.EntityFrameworkCore;
using Microsoft.OpenApi.Models;
using Services;
using Services.Entities;
using Services.Extension;
using Services.Services;
using Services.Services.Interfaces;
using Services.Utils;
using System.Reflection;
using System.Text.Json.Serialization;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

//Db context
builder.Services.AddDbContext<SilverJewelry2023DbContext>(x => x.UseSqlServer(builder.Configuration.GetConnectionString("db")));

//Json
builder.Services.AddControllers().AddJsonOptions(opt =>
{
    opt.JsonSerializerOptions.ReferenceHandler = ReferenceHandler.IgnoreCycles;
    opt.JsonSerializerOptions.DefaultIgnoreCondition = System.Text.Json.Serialization.JsonIgnoreCondition.WhenWritingNull;
});


//Dependency Injection
var jwtSection = builder.Configuration.GetSection("JWTSection").Get<JWTSection>();
builder.Services.AddSingleton(jwtSection);
builder.Services.AddScoped<IBranchAccountService, BranchAccountService>();
builder.Services.AddScoped<ISilverJewelryService, SilverJewelryService>();
builder.Services.AddScoped<IUnitOfWork, UnitOfWork>();

//AutoMapper
builder.Services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());

//Authentication and Authorization
builder.Services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
    .AddJwtBearer(JwtBearerDefaults.AuthenticationScheme, options => builder.Configuration.Bind("JWTSection", options));

builder.Services.AddAuthorization(options =>
{
    options.AddPolicy(MyUtils.IdentityData.Admin, policy => policy.RequireRole("1"));
    options.AddPolicy(MyUtils.IdentityData.Staff, policy => policy.RequireRole("2"));
});

#region SwaggerConfig
builder.Services.AddSwaggerGen(
        c =>
        {
            c.SwaggerDoc("v1", new OpenApiInfo
            {
                Title = "PE",
                Version = "v1",
                Description = "API",
                Contact = new OpenApiContact
                {
                    Url = new Uri("https://google.com")
                }
            });
            var xmlFileName = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
            c.IncludeXmlComments(Path.Combine(AppContext.BaseDirectory, xmlFileName));
            c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
            {
                Type = SecuritySchemeType.Http,
                In = ParameterLocation.Header,
                BearerFormat = "JWT",
                Scheme = "Bearer",
                Description = "Please input your token"
            });
            c.AddSecurityRequirement(new OpenApiSecurityRequirement
            {
                            {
                                new OpenApiSecurityScheme
                                {
                                    Reference=new OpenApiReference
                                    {
                                        Type=ReferenceType.SecurityScheme,
                                        Id="Bearer"
                                    }
                                },
                                new string[]{}
                            }
            });

        });
#endregion
var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

//app.UseMiddleware<JwtMiddleware>();

app.MapControllers();

app.Run();
